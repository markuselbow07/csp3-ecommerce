import { useState, Fragment } from 'react';
// Proptypes - used to validate props
import PropTypes from 'prop-types';
import { Button, Row, Col, Card, Modal, Badge } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import imageSpace from './Space.jpg';
import Swal from 'sweetalert2';



export default function ProductCard(prop, {data}){

	const {_id, name, description, price } = prop.productProp;
	const { setObject } = prop.data;
	console.log(data)

	const [count, setCount] = useState(0);
	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);


	function add(){
		// Find a way to pass productProp to Checkout function
		setCount(count + 1);
	}

	function subtract(){
		if(count > 0){
			setCount(count - 1);
		}

		else{
			Swal.fire({
				title: "Whoops",
				icon: "error",
				text: "Can't go down no more"
			})
		}
		
	}

	function addToCart(){
		Swal.fire({
				title: "Alright",
				icon: "Succes",
				text: "Items added to the cart"
			})
	}


	return (
			<Fragment>
				<Col xs={12} md={4} className="p-5">
					<Card className="cardHighlight border-0">
					
					<Card.Img className="image" onClick={handleShow} variant="top" src={imageSpace} />
					
					  <Card.Body className="">
					    <Card.Title className="text-center pb-4 title-card" onClick={handleShow}><h5>{name}</h5></Card.Title>
					    <Card.Text>
					      <div className="d-flex">      	
					      	
					      </div>

					     </Card.Text>
					      
					  </Card.Body>
					</Card>
				</Col>

				<Modal show={show} onHide={handleClose}>
				        <Modal.Header closeButton>
				          <Modal.Title>{name}</Modal.Title>
				        </Modal.Header>
				        <Modal.Body>
				        	<ul>
				        		<li><strong>Description:</strong></li>
				        		<li>{description}</li>
				        	</ul>

				        	<ul>
				        		<li><strong>Price:</strong></li>
				        		<li>PhP {price}</li>
				        	</ul>
				        </Modal.Body>
				        <Modal.Footer>
				          <Button variant="outline-warning" onClick={addToCart}><Badge variant="danger">{count}</Badge>{' '}Add to Cart!</Button>
				          <Button variant="outline-danger" onClick={subtract}>Subtract</Button>
				          <Button variant="outline-primary" onClick={add}>Add</Button>
				          <Button variant="outline-secondary" onClick={handleClose}>
				            Close
				          </Button>
				        </Modal.Footer>
				      </Modal>
			</Fragment>
	)
}




// Checks the validity of the PropTypes
ProductCard.propTypes = {
	// "shape" method is used to check if a prop object conforms to a specific shape
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}
